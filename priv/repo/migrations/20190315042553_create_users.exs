defmodule MokaAuth.Repo.Migrations.CreateUsers do
  use Ecto.Migration

  def up do
    execute "CREATE SCHEMA auth;"

    create table(:users, prefix: :auth) do
      add :email, :text
      add :password, :text
      add :first_name, :text
      add :last_name, :text
      add :phone, :text
      add :dob, :utc_datetime
      add :avatar, :text
      add :status, :text
      add :reset_password_token, :text
      add :active_email_token, :text
      add :fb_id, :text
      add :fb_access_token, :text
      add :g_id, :text
      add :g_token, :text
      add :g_refresh_token, :text
      add :otp_token, :text
      add :gender, :text

      timestamps()
    end

    create unique_index(:users, [:email], prefix: :auth)
  end

  def down do
    drop table(:users, prefix: :auth)
    execute "DROP SCHEMA auth;"
  end
end
