defmodule MokaAuth.AccountTest do
  use MokaAuth.DataCase

  alias MokaAuth.Account

  describe "users" do
    alias MokaAuth.Account.User

    @valid_attrs %{
      active_email_token: "some active_email_token",
      avatar: "some avatar",
      dob: "2010-04-17T14:00:00Z",
      email: "some email",
      fb_access_token: "some fb_access_token",
      fb_id: "some fb_id",
      first_name: "some first_name",
      g_id: "some g_id",
      g_refresh_token: "some g_refresh_token",
      g_token: "some g_token",
      last_name: "some last_name",
      otp_token: "some otp_token",
      password: "some password",
      phone: "some phone",
      reset_password_token: "some reset_password_token",
      status: "some status",
      gender: "male"
    }
    @update_attrs %{
      active_email_token: "some updated active_email_token",
      avatar: "some updated avatar",
      dob: "2011-05-18T15:01:01Z",
      email: "some updated email",
      fb_access_token: "some updated fb_access_token",
      fb_id: "some updated fb_id",
      first_name: "some updated first_name",
      g_id: "some updated g_id",
      g_refresh_token: "some updated g_refresh_token",
      g_token: "some updated g_token",
      last_name: "some updated last_name",
      otp_token: "some updated otp_token",
      password: "some updated password",
      phone: "some updated phone",
      reset_password_token: "some updated reset_password_token",
      status: "some updated status",
      gender: "male"
    }
    @invalid_attrs %{
      active_email_token: nil,
      avatar: nil,
      dob: nil,
      email: nil,
      fb_access_token: nil,
      fb_id: nil,
      first_name: nil,
      g_id: nil,
      g_refresh_token: nil,
      g_token: nil,
      last_name: nil,
      otp_token: nil,
      password: nil,
      phone: nil,
      reset_password_token: nil,
      status: nil,
      gender: "male"
    }

    def user_fixture(attrs \\ %{}) do
      {:ok, user} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Account.create_user()

      user
    end

    test "list_users/0 returns all users" do
      user = user_fixture()
      assert Account.list_users() == [user]
    end

    test "get_user!/1 returns the user with given id" do
      user = user_fixture()
      assert Account.get_user!(user.id) == user
    end

    test "create_user/1 with valid data creates a user" do
      assert {:ok, %User{} = user} = Account.create_user(@valid_attrs)
      assert user.active_email_token == "some active_email_token"
      assert user.avatar == "some avatar"
      assert user.dob == DateTime.from_naive!(~N[2010-04-17T14:00:00Z], "Etc/UTC")
      assert user.email == "some email"
      assert user.fb_access_token == "some fb_access_token"
      assert user.fb_id == "some fb_id"
      assert user.first_name == "some first_name"
      assert user.g_id == "some g_id"
      assert user.g_refresh_token == "some g_refresh_token"
      assert user.g_token == "some g_token"
      assert user.last_name == "some last_name"
      assert user.otp_token == "some otp_token"
      assert user.password == "some password"
      assert user.phone == "some phone"
      assert user.reset_password_token == "some reset_password_token"
      assert user.status == "some status"
      assert user.gender == "male"
    end

    test "create_user/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Account.create_user(@invalid_attrs)
    end

    test "update_user/2 with valid data updates the user" do
      user = user_fixture()
      assert {:ok, %User{} = user} = Account.update_user(user, @update_attrs)
      assert user.active_email_token == "some updated active_email_token"
      assert user.avatar == "some updated avatar"
      assert user.dob == DateTime.from_naive!(~N[2011-05-18T15:01:01Z], "Etc/UTC")
      assert user.email == "some updated email"
      assert user.fb_access_token == "some updated fb_access_token"
      assert user.fb_id == "some updated fb_id"
      assert user.first_name == "some updated first_name"
      assert user.g_id == "some updated g_id"
      assert user.g_refresh_token == "some updated g_refresh_token"
      assert user.g_token == "some updated g_token"
      assert user.last_name == "some updated last_name"
      assert user.otp_token == "some updated otp_token"
      assert user.password == "some updated password"
      assert user.phone == "some updated phone"
      assert user.reset_password_token == "some updated reset_password_token"
      assert user.status == "some updated status"
      assert user.gender == "male"
    end

    test "update_user/2 with invalid data returns error changeset" do
      user = user_fixture()
      assert {:error, %Ecto.Changeset{}} = Account.update_user(user, @invalid_attrs)
      assert user == Account.get_user!(user.id)
    end

    test "delete_user/1 deletes the user" do
      user = user_fixture()
      assert {:ok, %User{}} = Account.delete_user(user)
      assert_raise Ecto.NoResultsError, fn -> Account.get_user!(user.id) end
    end

    test "change_user/1 returns a user changeset" do
      user = user_fixture()
      assert %Ecto.Changeset{} = Account.change_user(user)
    end
  end
end
