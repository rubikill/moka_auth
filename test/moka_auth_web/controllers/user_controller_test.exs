defmodule MokaAuthWeb.UserControllerTest do
  use MokaAuthWeb.ConnCase

  alias MokaAuth.Account

  @create_attrs %{
    active_email_token: "some active_email_token",
    avatar: "some avatar",
    dob: "2010-04-17T14:00:00Z",
    email: "some email",
    fb_access_token: "some fb_access_token",
    fb_id: "some fb_id",
    first_name: "some first_name",
    g_id: "some g_id",
    g_refresh_token: "some g_refresh_token",
    g_token: "some g_token",
    last_name: "some last_name",
    otp_token: "some otp_token",
    password: "some password",
    phone: "some phone",
    reset_password_token: "some reset_password_token",
    status: "some status"
  }
  @update_attrs %{
    active_email_token: "some updated active_email_token",
    avatar: "some updated avatar",
    dob: "2011-05-18T15:01:01Z",
    email: "some updated email",
    fb_access_token: "some updated fb_access_token",
    fb_id: "some updated fb_id",
    first_name: "some updated first_name",
    g_id: "some updated g_id",
    g_refresh_token: "some updated g_refresh_token",
    g_token: "some updated g_token",
    last_name: "some updated last_name",
    otp_token: "some updated otp_token",
    password: "some updated password",
    phone: "some updated phone",
    reset_password_token: "some updated reset_password_token",
    status: "some updated status"
  }
  @invalid_attrs %{
    active_email_token: nil,
    avatar: nil,
    dob: nil,
    email: nil,
    fb_access_token: nil,
    fb_id: nil,
    first_name: nil,
    g_id: nil,
    g_refresh_token: nil,
    g_token: nil,
    last_name: nil,
    otp_token: nil,
    password: nil,
    phone: nil,
    reset_password_token: nil,
    status: nil
  }

  def fixture(:user) do
    {:ok, user} = Account.create_user(@create_attrs)
    user
  end

  describe "index" do
    test "lists all users", %{conn: conn} do
      conn = get(conn, Routes.user_path(conn, :index))
      assert html_response(conn, 200) =~ "Listing Users"
    end
  end

  describe "new user" do
    test "renders form", %{conn: conn} do
      conn = get(conn, Routes.user_path(conn, :new))
      assert html_response(conn, 200) =~ "New User"
    end
  end

  describe "create user" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post(conn, Routes.user_path(conn, :create), user: @create_attrs)

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == Routes.user_path(conn, :show, id)

      conn = get(conn, Routes.user_path(conn, :show, id))
      assert html_response(conn, 200) =~ "Show User"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.user_path(conn, :create), user: @invalid_attrs)
      assert html_response(conn, 200) =~ "New User"
    end
  end

  describe "edit user" do
    setup [:create_user]

    test "renders form for editing chosen user", %{conn: conn, user: user} do
      conn = get(conn, Routes.user_path(conn, :edit, user))
      assert html_response(conn, 200) =~ "Edit User"
    end
  end

  describe "update user" do
    setup [:create_user]

    test "redirects when data is valid", %{conn: conn, user: user} do
      conn = put(conn, Routes.user_path(conn, :update, user), user: @update_attrs)
      assert redirected_to(conn) == Routes.user_path(conn, :show, user)

      conn = get(conn, Routes.user_path(conn, :show, user))
      assert html_response(conn, 200) =~ "some updated active_email_token"
    end

    test "renders errors when data is invalid", %{conn: conn, user: user} do
      conn = put(conn, Routes.user_path(conn, :update, user), user: @invalid_attrs)
      assert html_response(conn, 200) =~ "Edit User"
    end
  end

  describe "delete user" do
    setup [:create_user]

    test "deletes chosen user", %{conn: conn, user: user} do
      conn = delete(conn, Routes.user_path(conn, :delete, user))
      assert redirected_to(conn) == Routes.user_path(conn, :index)

      assert_error_sent 404, fn ->
        get(conn, Routes.user_path(conn, :show, user))
      end
    end
  end

  defp create_user(_) do
    user = fixture(:user)
    {:ok, user: user}
  end
end
