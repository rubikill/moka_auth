defmodule MokaAuth do
  @moduledoc """
  MokaAuth keeps the contexts that define your domain
  and business logic.

  Contexts are also responsible for managing your data, regardless
  if it comes from the database, an external API or others.
  """

  def sign_in_by_phone(params) do
    MokaAuth.Account.sign_in_by_phone(params)
  end
end
