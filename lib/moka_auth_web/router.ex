defmodule MokaAuthWeb.Router do
  use MokaAuthWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", MokaAuthWeb do
    pipe_through :browser

    get "/", PageController, :index

    resources "/users", UserController

    get "/sign_in", PageController, :sign_in
    post "/sign_in", PageController, :do_sign_in
    get "/sign_up", PageController, :sign_up
    post "/sign_up", PageController, :do_sign_up
    get "/activate_by_email_token", PageController, :activate_by_email_token
  end

  # Other scopes may use custom stacks.
  # scope "/api", MokaAuthWeb do
  #   pipe_through :api
  # end
end
