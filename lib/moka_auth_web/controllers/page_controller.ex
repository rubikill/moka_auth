defmodule MokaAuthWeb.PageController do
  use MokaAuthWeb, :controller
  require Logger
  alias MokaAuth.Account
  alias MokaAuth.Account.User

  def index(conn, _params) do
    conn
    |> render("index.html")
  end

  def sign_in(conn, _params) do
    conn
    |> assign(:changeset, User.sign_in_by_email_changeset(%User{}, %{}))
    |> render("sign_in.html")
  end

  def do_sign_in(conn, params) do
    with {:ok, user} <- Account.sign_in_by_email(params["user"]) do
      conn
      |> put_flash(:info, "Sign in successfully")
      |> assign(:user, user)
      |> redirect(to: Routes.page_path(conn, :index))
    else
      {:error, :validation_fail, changeset} ->
        conn
        |> assign(:changeset, changeset)
        |> render("sign_in.html")

      {:error, :request_invalid, message} ->
        conn
        |> put_flash(:error, message)
        |> assign(:changeset, User.sign_in_by_email_changeset(%User{}, params["user"]))
        |> render("sign_in.html")

      err ->
        Logger.error("#{inspect(err)}")

        conn
        |> put_flash(:error, "Internal Server error")
        |> put_view(MokaAuthWeb.ErrorView)
        |> render("500.html")
    end
  end

  def sign_up(conn, _params) do
    conn
    |> assign(:changeset, User.sign_up_by_email_changeset(%User{}, %{}))
    |> render("sign_up.html")
  end

  def do_sign_up(conn, params) do
    with {:ok, user} <- Account.sign_up_by_email(params["user"]) do
      conn
      |> put_flash(:info, "Sign up successfully")
      |> assign(:user, user)
      |> redirect(to: Routes.page_path(conn, :index))
    else
      {:error, :validation_fail, changeset} ->
        conn
        |> assign(:changeset, changeset)
        |> render("sign_up.html")

      {:error, :request_invalid, message} ->
        conn
        |> put_flash(:error, message)
        |> assign(:changeset, User.sign_up_by_email_changeset(%User{}, params["user"]))
        |> render("sign_up.html")

      err ->
        Logger.error("#{inspect(err)}")

        conn
        |> put_flash(:error, "Internal Server error")
        |> put_view(MokaAuthWeb.ErrorView)
        |> render("500.html")
    end
  end

  def activate_by_email_token(conn, params) do
    with {:ok, user} <- Account.active_user_by_email(params) do
      conn
      |> put_flash(:info, "Activate user successfully")
      |> render("index.html")
    else
      {:error, :validation_fail, changeset} ->
        conn
        |> assign(:changeset, changeset)
        |> render("index.html")

      {:error, :request_invalid, message} ->
        conn
        |> put_flash(:error, message)
        |> assign(:changeset, User.active_by_email_changeset(%User{}, params))
        |> render("index.html")

      err ->
        Logger.error("#{inspect(err)}")

        conn
        |> put_flash(:error, "Internal Server error")
        |> put_view(MokaAuthWeb.ErrorView)
        |> render("500.html")
    end
  end
end
