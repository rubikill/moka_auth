defmodule MokaAuth.Account do
  @moduledoc """
  The Account context.
  """

  import Ecto.Query, warn: false
  require Logger
  alias MokaAuth.Repo

  alias MokaAuth.Account.User

  @doc """
  Returns the list of users.

  ## Examples

      iex> list_users()
      [%User{}, ...]

  """
  def list_users do
    Repo.all(User)
  end

  def list_users_with_paging(params) do
    from(i in User)
    |> MokaCommon.Filter.apply(params)
    |> MokaCommon.Paginator.new(Repo, params)
  end

  def list_users_with_loadmore(params) do
    Repo.all(User)
  end

  @doc """
  Gets a single user.

  Raises `Ecto.NoResultsError` if the User does not exist.

  ## Examples

      iex> get_user!(123)
      %User{}

      iex> get_user!(456)
      ** (Ecto.NoResultsError)

  """
  def get_user!(id), do: Repo.get!(User, id)

  @doc """
  Creates a user.

  ## Examples

      iex> create_user(%{field: value})
      {:ok, %User{}}

      iex> create_user(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_user(attrs \\ %{}) do
    %User{}
    |> User.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a user.

  ## Examples

      iex> update_user(user, %{field: new_value})
      {:ok, %User{}}

      iex> update_user(user, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_user(%User{} = user, attrs) do
    user
    |> User.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a User.

  ## Examples

      iex> delete_user(user)
      {:ok, %User{}}

      iex> delete_user(user)
      {:error, %Ecto.Changeset{}}

  """
  def delete_user(%User{} = user) do
    Repo.delete(user)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking user changes.

  ## Examples

      iex> change_user(user)
      %Ecto.Changeset{source: %User{}}

  """
  def change_user(%User{} = user) do
    User.changeset(user, %{})
  end

  @doc """
  Generate a random string with given length

  iex(1)> Share.CommonUtils.gen_code 6
  "e1vmt2"
  """
  def gen_code(l) do
    :rand.seed(:exsplus, :os.timestamp())
    alphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
    alphabet_length = 62

    Enum.map_join(1..l, fn _ ->
      String.at(alphabet, :rand.uniform(alphabet_length) - 1)
    end)
  end

  @doc """
  Generate a random string contains only number with given length

  iex(1)> Share.CommonUtils.gen_code 6
  "e1vmt2"
  """
  def gen_number(l) do
    :rand.seed(:exsplus, :os.timestamp())
    alphabet = "0123456789"
    alphabet_length = 10

    Enum.map_join(1..l, fn _ ->
      String.at(alphabet, :rand.uniform(alphabet_length) - 1)
    end)
  end

  def sign_up_by_email(params) do
    changeset =
      User.sign_up_by_email_changeset(
        %User{
          active_email_token: gen_code(64)
        },
        params
      )

    with {:ok, inserted_user} <- Repo.insert(changeset) do
      spawn(MokaAuth.Email, :welcome_mail, [[to: inserted_user.email]])
      {:ok, inserted_user}
    else
      {:error, changeset} ->
        {:error, :validation_fail, changeset}
    end
  end

  def active_user_by_email(params) do
    changeset = User.active_by_email_changeset(%User{}, params)

    if changeset.valid? do
      data = Ecto.Changeset.apply_changes(changeset)
      user = Repo.get_by(User, active_email_token: data.active_email_token)

      if is_nil(user) do
        {:error, :request_invalid, "Token is invalid"}
      else
        user
        |> User.changeset(params)
        |> Ecto.Changeset.put_change(:status, User.status_actived())
        |> Ecto.Changeset.put_change(:active_email_token, nil)
        |> Repo.update()
      end
    else
      {:error, :validation_fail, changeset}
    end
  end

  def sign_in_by_email(params) do
    changeset = User.sign_in_by_email_changeset(%User{}, params)

    if changeset.valid? do
      data = Ecto.Changeset.apply_changes(changeset)
      user = Repo.get_by(User, email: data.email, status: User.status_actived())

      if is_nil(user) do
        {:error, :request_invalid, "Email or password is not correct"}
      else
        if User.check_pw(user, data.password) do
          {:ok, user}
        else
          {:error, :request_invalid, "Email or password is not correct"}
        end
      end
    else
      {:error, :validation_fail, changeset}
    end
  end

  def sign_up_by_phone(params) do
    # otp_token
  end

  def sign_in_by_phone(params) do
    changeset = User.sign_in_by_phone_changeset(%User{}, params)

    if changeset.valid? do
      data = Ecto.Changeset.apply_changes(changeset)
      user = Repo.get_by(User, phone: data.phone)
      # if user.status ==
    else
      {:error, :validation_fail, changeset}
    end

    # with true <- changeset.valid?,
    #      data <- Ecto.Changeset.apply_changes(changeset),
    #      {:ok, user} <- User.check_pw(user, data.password) do
    #   if Abilities.can?(user, :sign_in, __MODULE__) do
    #     {:ok, user}
    #   else
    #     {:error, :request_invalid, "User it not actived yet"}
    #   end
    # end
  end

  def sign_in_by_facebook(params) do
  end

  def sign_in_by_google(params) do
  end

  def forgot_password(params) do
  end
end
