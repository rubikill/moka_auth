defmodule MokaAuth.Account.User do
  use Ecto.Schema
  import Ecto.Changeset
  require Logger

  @email_regex ~r/[a-zA-Z0-9\-_.+]+@[a-zA-Z0-9_\-.]+\..+/
  @phone_regex ~r/\b\d{3}[-.]?\d{3}[-.]?\d{4,5}\b/
  @default_date_format "{YYYY}-{0M}-{0D}"
  @default_datetime_format "{YYYY}-{0M}-{0D}T{h24}:{m}:{s}Z"

  def status_actived, do: "actived"
  def status_inactived, do: "inactived"

  def status_enum,
    do: [
      "pending",
      "actived",
      "inactived"
    ]

  def gender_male, do: "male"
  def gender_female, do: "female"
  def gender_other, do: "other"

  def gender_enum,
    do: [
      "male",
      "female",
      "other"
    ]

  @schema_prefix :auth
  schema "users" do
    field :active_email_token, :string
    field :avatar, :string
    field :dob, :utc_datetime
    field :email, :string
    field :fb_access_token, :string
    field :fb_id, :string
    field :first_name, :string
    field :g_id, :string
    field :g_refresh_token, :string
    field :g_token, :string
    field :last_name, :string
    field :otp_token, :string
    field :password, :string
    field :phone, :string
    field :reset_password_token, :string
    field :status, :string, default: "pending"
    field :gender, :string

    timestamps()
  end

  @required_fields [
    :status,
    :email
  ]
  @optional_fields [
    :password,
    :first_name,
    :last_name,
    :phone,
    :dob,
    :avatar,
    :reset_password_token,
    :active_email_token,
    :fb_id,
    :fb_access_token,
    :g_id,
    :g_token,
    :g_refresh_token,
    :otp_token,
    :gender
  ]
  @doc false
  def changeset(user, params) do
    user
    |> cast(params, @required_fields ++ @optional_fields)
    |> validate_required(@required_fields)
    |> validate_format(:email, @email_regex)
    |> validate_format(:phone, @phone_regex)
    |> validate_inclusion(:gender, gender_enum())
    |> validate_inclusion(:status, status_enum())
    |> unique_constraint(:email)
  end

  def sign_up_by_email_changeset(user, params) do
    user
    |> cast(params, [:email, :password, :first_name, :last_name, :phone, :dob, :gender])
    |> validate_required([:email, :password])
    |> validate_format(:email, @email_regex)
    |> validate_format(:phone, @phone_regex)
    |> validate_inclusion(:gender, gender_enum())
    |> hashpw()
    |> unique_constraint(:email)
  end

  def active_by_email_changeset(user, params) do
    user
    |> cast(params, [:active_email_token, :email, :first_name, :last_name, :phone, :dob, :gender])
    |> validate_required([:active_email_token, :email])
    |> validate_format(:email, @email_regex)
    |> validate_format(:phone, @phone_regex)
    |> validate_inclusion(:gender, gender_enum())
  end

  def sign_in_by_email_changeset(user, params) do
    user
    |> cast(params, [:email, :password])
    |> validate_required([:email, :password])
    |> validate_format(:email, @email_regex)
  end

  def sign_up_by_phone_changeset(user, params) do
    user
    |> cast(params, [:email, :password, :first_name, :last_name, :phone, :dob, :gender])
    |> validate_required([:phone, :password])
    |> generate_email(:phone)
    |> validate_format(:email, @email_regex)
    |> validate_format(:phone, @phone_regex)
    |> validate_inclusion(:gender, gender_enum())
    |> unique_constraint(:email)
    |> hashpw()
  end

  def sign_in_by_phone_changeset(user, params) do
    user
    |> cast(params, [:phone, :password])
    |> validate_required([:phone, :password])
    |> validate_format(:phone, @phone_regex)
  end

  def sign_up_by_fb_changeset(user, params) do
    user
    |> cast(params, [:email, :first_name, :last_name, :phone, :dob, :gender])
    |> validate_required([:fb_id, :fb_access_token])
    |> generate_email(:fb_id)
    |> validate_format(:email, @email_regex)
    |> validate_format(:phone, @phone_regex)
    |> validate_inclusion(:gender, gender_enum())
    |> unique_constraint(:email)
  end

  defp generate_email(changeset, field) do
    generated_email = get_change(changeset, field) <> "@yeah1.com"

    if generated_email do
      put_change(changeset, :email, generated_email)
    else
      changeset
    end
  end

  defp hashpw(changeset) do
    raw_password = get_change(changeset, :password)

    if raw_password do
      password = Comeonin.Bcrypt.hashpwsalt(raw_password)
      put_change(changeset, :password, password)
    else
      changeset
    end
  end

  def check_pw(user = %__MODULE__{}, password) do
    if Comeonin.Bcrypt.checkpw(password, user.password) do
      {:ok, user}
    else
      {:error, :request_invalid, "Password not match"}
    end
  end

  def parse_date(changeset, field, params, format \\ @default_date_format)

  def parse_date(changeset, field, params, format) do
    parse(changeset, field, params, format)
  end

  def parse_datetime(changeset, field, params, format \\ @default_datetime_format)

  def parse_datetime(changeset, field, params, format) do
    parse(changeset, field, params, format)
  end

  defp parse(changeset, field, params, format) do
    value = Map.get(params, to_string(field))

    if is_nil(value) or value == "" do
      changeset
    else
      case Timex.parse(value, format) do
        {:ok, parsed_value} ->
          put_change(changeset, field, parsed_value)

        {:error, err} ->
          Logger.error("#{inspect(err)}")
          add_error(changeset, field, "Invalid format")
      end
    end
  end
end
