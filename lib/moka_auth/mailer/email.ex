defmodule MokaAuth.Email do
  # Define your emails
  import Bamboo.Email

  @from_default {"Noreply", "noreply@MokaAuth.com"}

  def welcome_mail(assigns) do
    to = assigns[:to]
    from = assigns[:from] || @from_default
    subject = assigns[:subject] || "Welcome email"

    html_body =
      assigns[:body] ||
        """
        Test mail
        """

    new_email(to: to, from: from, subject: subject, html_body: html_body)
    |> MokaAuth.Mailer.deliver_later()
  end
end
