# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :moka_auth,
  ecto_repos: [MokaAuth.Repo]

# Configures the endpoint
config :moka_auth, MokaAuthWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "oA0hJb/t7bxAWoZxmddqpvC7tgSWUyOZ9getsU7+UwHpX9UE+uLOSluCr7geMgMz",
  render_errors: [view: MokaAuthWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: MokaAuth.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

config :moka_auth, MokaAuth.Mailer, adapter: Bamboo.LocalAdapter
# adapter: Bamboo.SendGridAdapter,
# api_key: {:system, "SENDGRID_API_KEY"}

#
config :esms,
  api_key: System.get_env("ESMS_API_KEY"),
  secret_key: System.get_env("ESMS_SECRET_KEY"),
  brandname: System.get_env("ESMS_BRANDNAME"),
  sms_type: String.to_integer(System.get_env("ESMS_SMS_TYPE") || "2"),
  sandbox: String.to_integer(System.get_env("ESMS_SANDBOX") || "1")

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
